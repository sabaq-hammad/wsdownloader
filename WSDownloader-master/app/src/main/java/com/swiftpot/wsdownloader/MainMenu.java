package com.swiftpot.wsdownloader;

import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.Button;
import android.widget.LinearLayout;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;
import com.swiftpot.wsdownloader.adapter.RecyclerViewMenuStatusMediaAdapter;
import com.swiftpot.wsdownloader.base.BaseActivity;

import java.io.File;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainMenu extends BaseActivity {
    public static final String TAG = "Menu";
    private static final String WHATSAPP_STATUSES_LOCATION = "/WhatsApp/Media/.Statuses";
    @BindView(R.id.btnDownloadedStatus)
    LinearLayout btnSavedStatusesList;
    @BindView(R.id.btnRecentStatus)
    LinearLayout btnRecentStatuses;
    private RecyclerView mRecyclerViewMediaList;
    private LinearLayoutManager mLinearLayoutManager;
    private AdView mAdView;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_menu);
        ButterKnife.bind(this);
//AdMob

        MobileAds.initialize(this,
                "ca-app-pub-3940256099942544~3347511713");

        mAdView = findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);

        mRecyclerViewMediaList = (RecyclerView) findViewById(R.id.recyclerViewMedia);

        mLinearLayoutManager = new LinearLayoutManager(this);
        mLinearLayoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
        mRecyclerViewMediaList.setLayoutManager(mLinearLayoutManager);
        RecyclerViewMenuStatusMediaAdapter recyclerViewMediaAdapter = new RecyclerViewMenuStatusMediaAdapter(this.getListFiles(new File(Environment.getExternalStorageDirectory().toString() + WHATSAPP_STATUSES_LOCATION)), MainMenu.this);
        mRecyclerViewMediaList.setAdapter(recyclerViewMediaAdapter);
    }

    /**
     * get all the files in specified directory
     *
     * @param parentDir
     * @return
     */
    private ArrayList<File> getListFiles(File parentDir) {
        ArrayList<File> inFiles = new ArrayList<File>();
        File[] files;
        files = parentDir.listFiles();
        if (files != null) {
            for (File file : files) {

                if (file.getName().endsWith(".jpg") ||
                        file.getName().endsWith(".gif") ||
                        file.getName().endsWith(".mp4")) {
                    if (!inFiles.contains(file))
                        inFiles.add(file);
                }
            }
        }
        return inFiles;
    }

    @OnClick(R.id.btnDownloadedStatus)
    void showDownloadedList() {
        Intent intent = new Intent(MainMenu.this, SavedStatusesList.class);
        startActivity(intent);
    }

    @OnClick(R.id.btnRecentStatus)
    void showRecentList() {
        Intent intent = new Intent(MainMenu.this, HomeActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.btnTutorial)
    void showTutorial() {
        Intent intent = new Intent(MainMenu.this, Tutorial.class);
        startActivity(intent);
    }


}
