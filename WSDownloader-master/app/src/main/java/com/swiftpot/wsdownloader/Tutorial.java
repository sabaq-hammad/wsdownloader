package com.swiftpot.wsdownloader;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.shashank.sony.fancywalkthroughlib.FancyWalkthroughActivity;
import com.shashank.sony.fancywalkthroughlib.FancyWalkthroughCard;
import com.swiftpot.wsdownloader.base.BaseActivity;

import java.util.ArrayList;
import java.util.List;

import za.co.riggaroo.materialhelptutorial.TutorialItem;
import za.co.riggaroo.materialhelptutorial.tutorial.MaterialTutorialActivity;

public class Tutorial extends FancyWalkthroughActivity {

    private static final int REQUEST_CODE = 1234;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
       // setContentView(R.layout.activity_tuorial);
        loadTutorial();

    }

    @Override
    public void onFinishButtonPressed() {
        startActivity(new Intent(this,MainMenu.class));
        finish();
    }

    public void loadTutorial() {
        FancyWalkthroughCard fancywalkthroughCard1 = new FancyWalkthroughCard("Open Whatsapp and Watch Status.", "",R.drawable.wh);
        FancyWalkthroughCard fancywalkthroughCard2 = new FancyWalkthroughCard("Than Open this app, you can download & share status.", "",R.drawable.wh);

        //fancywalkthroughCard1.setBackgroundColor(R.color.white);
        fancywalkthroughCard1.setIconLayoutParams(900,900,0,0,0,0);

        // fancywalkthroughCard2.setBackgroundColor(R.color.white);
        fancywalkthroughCard2.setIconLayoutParams(900,900,0,0,0,0);

        List<FancyWalkthroughCard> pages = new ArrayList<>();

        pages.add(fancywalkthroughCard1);
        pages.add(fancywalkthroughCard2);


        for (FancyWalkthroughCard page : pages) {
            page.setTitleColor(R.color.black);
            fancywalkthroughCard2.setBackgroundColor(R.color.white);
            page.setDescriptionColor(R.color.black);
        }
        setFinishButtonTitle("Get Started");
        showNavigationControls(true);
        setColorBackground(R.color.colorMain);
        //setImageBackground(R.drawable.restaurant);
        setInactiveIndicatorColor(R.color.grey_600);
        setActiveIndicatorColor(R.color.colorPrimary);
        setOnboardPages(pages);
    }


}
