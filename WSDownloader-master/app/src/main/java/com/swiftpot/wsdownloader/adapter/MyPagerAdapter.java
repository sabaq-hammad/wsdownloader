package com.swiftpot.wsdownloader.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Environment;
import android.support.v4.view.PagerAdapter;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.MediaController;
import android.widget.RelativeLayout;
import android.widget.VideoView;

import com.swiftpot.wsdownloader.R;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.FileChannel;
import java.util.ArrayList;

import de.mateware.snacky.Snacky;

public class MyPagerAdapter extends PagerAdapter {

    private static String DIRECTORY_TO_SAVE_MEDIA_NOW = "/WSDownloader/";
    LayoutInflater mLayoutInflater;
    ArrayList<File> mResources;
    private ArrayList<File> filesList;
    private Activity activity;

    public MyPagerAdapter(Activity activity, ArrayList<File> mResources) {
        this.activity = activity;
        mLayoutInflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.mResources = mResources;

    }

    /**
     * copy file to destination.
     *
     * @param sourceFile
     * @param destFile
     * @throws IOException
     */
    public static void copyFile(File sourceFile, File destFile) throws IOException {
        if (!destFile.getParentFile().exists())
            destFile.getParentFile().mkdirs();

        if (!destFile.exists()) {
            destFile.createNewFile();
        }

        FileChannel source = null;
        FileChannel destination = null;

        try {
            source = new FileInputStream(sourceFile).getChannel();
            destination = new FileOutputStream(destFile).getChannel();
            destination.transferFrom(source, 0, source.size());
        } finally {
            if (source != null) {
                source.close();
            }
            if (destination != null) {
                destination.close();
            }
        }
    }

    @Override
    public int getCount() {
        return mResources.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == ((RelativeLayout) object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        View itemView = mLayoutInflater.inflate(R.layout.pager_item, container, false);
        File currentFile = mResources.get(position);

        ImageView imageViewImageMedia
                = (ImageView) itemView.findViewById(R.id.imageViewImageMedia);
        VideoView videoViewVideoMedia
                = (VideoView) itemView.findViewById(R.id.videoViewVideoMedia);
        MediaController mediaController = new MediaController(activity);
        mediaController.setAnchorView(videoViewVideoMedia);

        videoViewVideoMedia.setMediaController(mediaController);
        CardView cardViewVideoMedia = (CardView) itemView.findViewById(R.id.cardViewVideoMedia);
        CardView cardViewImageMedia = (CardView) itemView.findViewById(R.id.cardViewImageMedia);
        ImageButton buttonImageDownload
                = (ImageButton) itemView.findViewById(R.id.buttonImageDownload);
        ImageButton buttonVideoDownload
                = (ImageButton) itemView.findViewById(R.id.buttonVideoDownload);
        ImageButton buttonVideoShare
                = (ImageButton) itemView.findViewById(R.id.buttonVideoShare);
        ImageButton buttonImageShare
                = (ImageButton) itemView.findViewById(R.id.buttonImageShare);

        buttonImageDownload.setOnClickListener(this.downloadMediaItem(currentFile));
        buttonVideoDownload.setOnClickListener(this.downloadMediaItem(currentFile));


        buttonImageShare.setOnClickListener(this.share(currentFile));
        buttonVideoShare.setOnClickListener(this.share(currentFile));
        if (currentFile.getAbsolutePath().endsWith(".mp4")) {
            cardViewImageMedia.setVisibility(View.GONE);
            cardViewVideoMedia.setVisibility(View.VISIBLE);
            Uri video = Uri.parse(currentFile.getAbsolutePath());
            videoViewVideoMedia.setVideoURI(video);

            videoViewVideoMedia.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mp) {
                    // mp.setLooping(true);
                    // videoViewVideoMedia.start();
                }
            });
        } else {
            cardViewImageMedia.setVisibility(View.VISIBLE);
            cardViewVideoMedia.setVisibility(View.GONE);
            Bitmap myBitmap = BitmapFactory.decodeFile(currentFile.getAbsolutePath());
            imageViewImageMedia.setImageBitmap(myBitmap);
        }

        container.addView(itemView);

        return itemView;
    }

    public View.OnClickListener downloadMediaItem(final File sourceFile) {

        return new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                new Runnable() {

                    @Override
                    public void run() {
                        try {
                            copyFile(sourceFile, new File(Environment.getExternalStorageDirectory().toString() + DIRECTORY_TO_SAVE_MEDIA_NOW + sourceFile.getName()));
                            Snacky.builder().
                                    setActivty(activity).
                                    setText(R.string.save_successful_message).
                                    success().
                                    show();
                        } catch (Exception e) {
                            e.printStackTrace();
                            Log.e("RecyclerV", "onClick: Error:" + e.getMessage());

                            Snacky.builder().
                                    setActivty(activity).
                                    setText(R.string.save_error_message).
                                    error().
                                    show();
                        }
                    }
                }.run();
            }
        };
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((RelativeLayout) object);
    }

    public View.OnClickListener share(final File file) {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent share = new Intent(Intent.ACTION_SEND);
                share.setType("image/jpeg");
                share.setType("video/mp4");
                share.putExtra(Intent.EXTRA_STREAM, Uri.parse(file.toString()));
                activity.startActivity(Intent.createChooser(share, "Share Image"));
            }
        };
    }
}