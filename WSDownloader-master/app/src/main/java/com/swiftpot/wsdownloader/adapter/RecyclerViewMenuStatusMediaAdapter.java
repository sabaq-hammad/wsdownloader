package com.swiftpot.wsdownloader.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.MediaController;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.swiftpot.wsdownloader.R;
import com.swiftpot.wsdownloader.StatusDetail;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.FileChannel;
import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;
import de.mateware.snacky.Snacky;

/**
 * Created by Ace Programmer Rbk<rodney@swiftpot.com> on 06-May-17
 * 8:24 AM
 */
public class RecyclerViewMenuStatusMediaAdapter extends RecyclerView.Adapter<RecyclerViewMenuStatusMediaAdapter.FileHolder> {

    private static String DIRECTORY_TO_SAVE_MEDIA_NOW = "/WSDownloader/";
    private ArrayList<File> filesList;
    private Activity activity;

    public RecyclerViewMenuStatusMediaAdapter(ArrayList<File> filesList, Activity activity) {
        this.filesList = filesList;
        this.activity = activity;
        setHasStableIds(true);
    }

    /**
     * copy file to destination.
     *
     * @param sourceFile
     * @param destFile
     * @throws IOException
     */
    public static void copyFile(File sourceFile, File destFile) throws IOException {
        if (!destFile.getParentFile().exists())
            destFile.getParentFile().mkdirs();

        if (!destFile.exists()) {
            destFile.createNewFile();
        }

        FileChannel source = null;
        FileChannel destination = null;

        try {
            source = new FileInputStream(sourceFile).getChannel();
            destination = new FileOutputStream(destFile).getChannel();
            destination.transferFrom(source, 0, source.size());
        } finally {
            if (source != null) {
                source.close();
            }
            if (destination != null) {
                destination.close();
            }
        }
    }

    @Override
    public RecyclerViewMenuStatusMediaAdapter.FileHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View inflatedView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recyclerview_home_status_item, parent, false);
        return new FileHolder(inflatedView, activity);
    }

    @Override
    public void onBindViewHolder(final RecyclerViewMenuStatusMediaAdapter.FileHolder holder, int position) {
        File currentFile = filesList.get(position);


        if (currentFile.getAbsolutePath().endsWith(".mp4")) {
            holder.imageViewImageMedia.setVisibility(View.GONE);
            holder.videoViewVideoMedia.setVisibility(View.VISIBLE);
            Uri video = Uri.parse(currentFile.getAbsolutePath());
            Bitmap thumb = ThumbnailUtils.createVideoThumbnail(String.valueOf(currentFile), MediaStore.Images.Thumbnails.MINI_KIND);

            Glide
                    .with(activity)
                    .load(thumb)
                    .into(holder.videoViewVideoMedia);
            //holder.videoViewVideoMedia.setImageBitmap(thumb);


        } else {
            holder.imageViewImageMedia.setVisibility(View.VISIBLE);
            holder.videoViewVideoMedia.setVisibility(View.GONE);
            Bitmap myBitmap = BitmapFactory.decodeFile(currentFile.getAbsolutePath());
            Glide
                    .with(activity)
                    .load(myBitmap)
                    .into(holder.imageViewImageMedia);
//            holder.imageViewImageMedia.setImageBitmap(myBitmap);
        }

    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return filesList.size();
    }

    public View.OnClickListener downloadMediaItem(final File sourceFile) {

        return new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                new Runnable() {

                    @Override
                    public void run() {
                        try {
                            copyFile(sourceFile, new File(Environment.getExternalStorageDirectory().toString() + DIRECTORY_TO_SAVE_MEDIA_NOW + sourceFile.getName()));
                            Snacky.builder().
                                    setActivty(activity).
                                    setText(R.string.save_successful_message).
                                    success().
                                    show();
                        } catch (Exception e) {
                            e.printStackTrace();
                            Log.e("RecyclerV", "onClick: Error:" + e.getMessage());

                            Snacky.builder().
                                    setActivty(activity).
                                    setText(R.string.save_error_message).
                                    error().
                                    show();
                        }
                    }
                }.run();
            }
        };
    }

    public static class FileHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        CircleImageView imageViewImageMedia;
        CircleImageView videoViewVideoMedia;

        public FileHolder(View itemView, Activity activity) {
            super(itemView);
            imageViewImageMedia = (CircleImageView) itemView.findViewById(R.id.imageViewImageMedia);
            videoViewVideoMedia = (CircleImageView) itemView.findViewById(R.id.videoViewVideoMedia);
            MediaController mediaController = new MediaController(activity);
            mediaController.setAnchorView(videoViewVideoMedia);

//            videoViewVideoMedia.setMediaController(mediaController);

            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            Context context = v.getContext();

            Intent i = new Intent(context, StatusDetail.class);
            String filePath = null;
            i.putExtra("FilePath", filePath);
            v.getContext().startActivity(i);
        }

    }
}
