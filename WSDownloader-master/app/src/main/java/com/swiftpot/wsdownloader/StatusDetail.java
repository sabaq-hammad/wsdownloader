package com.swiftpot.wsdownloader;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;

import com.swiftpot.wsdownloader.adapter.MyPagerAdapter;

import java.io.File;
import java.util.ArrayList;

public class StatusDetail extends AppCompatActivity {
    private static final String WHATSAPP_STATUSES_LOCATION = "/WhatsApp/Media/.Statuses";
    ViewPager viewPager;
    MyPagerAdapter pagerAdapter;
    Intent intent;
    Activity context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_status_detail);
        context = this;
        viewPager = (ViewPager) findViewById(R.id.viewPager);

        pagerAdapter = new MyPagerAdapter(context,this.getListFiles(new File(Environment.getExternalStorageDirectory().toString() + WHATSAPP_STATUSES_LOCATION)));
        viewPager.setAdapter(pagerAdapter);


        CubeOutRotationTransformation cubeOutRotationTransformation = new CubeOutRotationTransformation();
        intent = getIntent();
        viewPager.setPageTransformer(true, cubeOutRotationTransformation);
    }

    private ArrayList<File> getListFiles(File parentDir) {
        ArrayList<File> inFiles = new ArrayList<File>();
        File[] files;
        files = parentDir.listFiles();
        if (files != null) {
            for (File file : files) {

                if (file.getName().endsWith(".jpg") ||
                        file.getName().endsWith(".gif") ||
                        file.getName().endsWith(".mp4")) {
                    if (!inFiles.contains(file))
                        inFiles.add(file);
                }
            }
        }
        return inFiles;
    }

}
