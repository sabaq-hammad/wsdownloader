package com.swiftpot.wsdownloader.adapter;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Environment;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.MediaController;
import android.widget.VideoView;

import com.bumptech.glide.Glide;
import com.swiftpot.wsdownloader.R;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.FileChannel;
import java.util.ArrayList;

import de.mateware.snacky.Snacky;

/**
 * Created by Ace Programmer Rbk<rodney@swiftpot.com> on 06-May-17
 * 8:24 AM
 */
public class SavedStatusesRecyclerViewMediaAdapter extends RecyclerView.Adapter<SavedStatusesRecyclerViewMediaAdapter.FileHolder> {

    private static String DIRECTORY_TO_SAVE_MEDIA_NOW = "/WSDownloader/";
    private ArrayList<File> filesList;
    private Activity activity;

    public SavedStatusesRecyclerViewMediaAdapter(ArrayList<File> filesList, Activity activity) {
        this.filesList = filesList;
        this.activity = activity;
        setHasStableIds(true);
    }

    @Override
    public SavedStatusesRecyclerViewMediaAdapter.FileHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View inflatedView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recyclerview_media_row_item, parent, false);
        return new FileHolder(inflatedView, activity);
    }

    @Override
    public void onBindViewHolder(final SavedStatusesRecyclerViewMediaAdapter.FileHolder holder, int position) {
        File currentFile = filesList.get(position);

        holder.buttonImageDownload.setOnClickListener(this.downloadMediaItem(currentFile));
        holder.buttonVideoDownload.setOnClickListener(this.downloadMediaItem(currentFile));

        if (currentFile.getAbsolutePath().endsWith(".mp4")) {
            holder.cardViewImageMedia.setVisibility(View.GONE);
            holder.cardViewVideoMedia.setVisibility(View.VISIBLE);
            Uri video = Uri.parse(currentFile.getAbsolutePath());
            holder.videoViewVideoMedia.setVideoURI(video);

            holder.videoViewVideoMedia.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mp) {
                   // mp.setLooping(true);
                   // holder.videoViewVideoMedia.start();
                }
            });
        } else {
            holder.cardViewImageMedia.setVisibility(View.VISIBLE);
            holder.cardViewVideoMedia.setVisibility(View.GONE);
            Bitmap myBitmap = BitmapFactory.decodeFile(currentFile.getAbsolutePath());
            Glide
                    .with(activity)
                    .load(myBitmap)
                    .into(holder.imageViewImageMedia);
            //holder.imageViewImageMedia.setImageBitmap(myBitmap);
        }

    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return filesList.size();
    }

    public View.OnClickListener downloadMediaItem(final File sourceFile) {

        return new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                new Runnable() {

                    @Override
                    public void run() {
                        try {
                            copyFile(sourceFile, new File(Environment.getExternalStorageDirectory().toString() + DIRECTORY_TO_SAVE_MEDIA_NOW + sourceFile.getName()));
                            Snacky.builder().
                                    setActivty(activity).
                                    setText(R.string.save_successful_message).
                                    success().
                                    show();
                        } catch (Exception e) {
                            e.printStackTrace();
                            Log.e("RecyclerV", "onClick: Error:" + e.getMessage());

                            Snacky.builder().
                                    setActivty(activity).
                                    setText(R.string.save_error_message).
                                    error().
                                    show();
                        }
                    }
                }.run();
            }
        };
    }

    /**
     * copy file to destination.
     *
     * @param sourceFile
     * @param destFile
     * @throws IOException
     */
    public static void copyFile(File sourceFile, File destFile) throws IOException {
        if (!destFile.getParentFile().exists())
            destFile.getParentFile().mkdirs();

        if (!destFile.exists()) {
            destFile.createNewFile();
        }

        FileChannel source = null;
        FileChannel destination = null;

        try {
            source = new FileInputStream(sourceFile).getChannel();
            destination = new FileOutputStream(destFile).getChannel();
            destination.transferFrom(source, 0, source.size());
        } finally {
            if (source != null) {
                source.close();
            }
            if (destination != null) {
                destination.close();
            }
        }
    }


    public static class FileHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        ImageView imageViewImageMedia;
        VideoView videoViewVideoMedia;
        CardView cardViewVideoMedia;
        CardView cardViewImageMedia;
        ImageView buttonVideoDownload;
        ImageView buttonImageDownload;

        public FileHolder(View itemView, Activity activity) {
            super(itemView);
            imageViewImageMedia = (ImageView) itemView.findViewById(R.id.imageViewImageMedia);
            videoViewVideoMedia = (VideoView) itemView.findViewById(R.id.videoViewVideoMedia);
            MediaController mediaController = new MediaController(activity);
            mediaController.setAnchorView(videoViewVideoMedia);

            videoViewVideoMedia.setMediaController(mediaController);
            cardViewVideoMedia = (CardView) itemView.findViewById(R.id.cardViewVideoMedia);
            cardViewImageMedia = (CardView) itemView.findViewById(R.id.cardViewImageMedia);
            buttonImageDownload = (ImageView) itemView.findViewById(R.id.buttonImageDownload);
            buttonVideoDownload = (ImageView) itemView.findViewById(R.id.buttonVideoDownload);
            buttonImageDownload.setVisibility(View.GONE);
            buttonVideoDownload.setVisibility(View.GONE);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {

        }

    }
}
